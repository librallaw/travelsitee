var gulp = require('gulp'),
watch = require('gulp-watch'),
    postcss = require ('post-css')

gulp.task('default',function (){
    console.log("Hurrah, you created a gulp task");
});

gulp.task('html', function (){

    console.log("Imagine creating something useful with Gulp ");
});

gulp.task('styles',function (){

    return gulp.src('./app/assets/styles.css').pipe(gulp.dest('./app/temp/styles'));

    //console.log('Styles have been changed');
})


gulp.task('watch', function (){
    
    watch("./app/index.html", function (){
        gulp.start('html')
    });


    watch("./app/assets/styles/**/*.css", function(){
        gulp.start('styles');
    });


})


